import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class Main {
    private static final Font helvetica = new Font(Font.FontFamily.HELVETICA, 40,
            Font.BOLD);

    public static void main(String[] args) throws FileNotFoundException, DocumentException {
        Document document = new Document(PageSize.A4, 50, 50, 50, 50);
        PdfWriter.getInstance(document, new FileOutputStream("result.pdf"));
        document.open();
        addTitle(document);
        addTable(document);
        document.close();
    }

    private static void addTitle(Document document) throws DocumentException{
        Paragraph paragraph = new Paragraph("Result", helvetica);
        paragraph.setAlignment(Element.ALIGN_CENTER);
        paragraph.setSpacingAfter(120);
        document.add(paragraph);
    }

    private static void addTable(Document document) throws DocumentException{
        PdfPTable table = new PdfPTable(2);
        table.addCell("First name :");
        table.addCell("Patryk");
        table.addCell("Lsat name :");
        table.addCell("Ptak");
        table.addCell("Profession :");
        table.addCell("Student");
        table.addCell("Education :");
        table.addCell("PWSZ Tarnów 2018-2021");
        table.addCell("Summary :");
        table.addCell("KEKW");
        document.add(table);
    }
}
